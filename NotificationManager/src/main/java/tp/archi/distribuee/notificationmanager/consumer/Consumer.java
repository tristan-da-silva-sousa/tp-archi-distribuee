package tp.archi.distribuee.notificationmanager.consumer;

import com.google.gson.Gson;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import tp.archi.distribuee.notificationmanager.model.Notification;

import java.time.LocalDateTime;
import java.util.logging.Logger;

@Component
public class Consumer {

    final static Logger logger = Logger.getLogger("Consumer");
    @RabbitListener(queues = "${notification.rabbitmq.queue}")
    public void receivedMessage(String msg){
        logger.info("Received Message: " + msg);
        Gson gson = new Gson();
        Notification notification = gson.fromJson(msg, Notification.class);
    }
}
