package tp.archi.distribuee.notificationmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationmanagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotificationmanagerApplication.class, args);
    }

}
