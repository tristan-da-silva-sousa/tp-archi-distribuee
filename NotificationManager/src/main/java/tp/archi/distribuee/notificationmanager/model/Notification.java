package tp.archi.distribuee.notificationmanager.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Notification {

    @Id
    private String Id;
    private String path;
    private String clientId;
    private Status status;



}

