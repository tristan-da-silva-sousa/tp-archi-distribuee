package tp.architecture.distribuee.back4front.endpoints;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tp.architecture.distribuee.back4front.model.Client;
import tp.architecture.distribuee.back4front.service.SubscriptionService;
import java.io.IOException;

@RestController
@RequestMapping("/back4front")
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @PostMapping("/subscribe")
    public String Subscribe(@RequestBody Client client) throws IOException, InterruptedException {
        String webSocketUrl = subscriptionService.getWebSocket(client.getClientId());
        String subscriptionResponse = subscriptionService.subscribe(client.getClientId(),webSocketUrl);
        return webSocketUrl;
    }

    @PostMapping("/convert/{id}")
    public ResponseEntity<String> convert(@PathVariable("id") int id, @RequestBody  String path) throws IOException, InterruptedException {
        subscriptionService.convert(id,path);

        return ResponseEntity.ok("Request send");
    }


}
