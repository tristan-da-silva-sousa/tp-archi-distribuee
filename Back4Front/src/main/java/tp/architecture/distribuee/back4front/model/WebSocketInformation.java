package tp.architecture.distribuee.back4front.model;

import lombok.Data;

@Data
public class WebSocketInformation {
    String clientId;
    String webSocketUrl;
}
