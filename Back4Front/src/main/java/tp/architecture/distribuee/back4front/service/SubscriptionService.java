package tp.architecture.distribuee.back4front.service;

import com.google.gson.Gson;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.http.converter.json.JsonbHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import tp.architecture.distribuee.back4front.model.WebSocketInformation;
import tp.architecture.distribuee.back4front.repository.SubscriptionRepository;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

@Service
@Component
public class SubscriptionService {

    private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .connectTimeout(Duration.ofSeconds(10))
            .build();

    SubscriptionRepository subscriptionrepository;

    public String getWebSocket(String clientId) throws IOException, InterruptedException {
        Gson gson = new Gson();
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(gson.toJson(clientId)))
                .uri(URI.create("http://localhost:5000/websocket/ws-admin/create"))
                .setHeader("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        String webSocketUrl = response.body();
        return webSocketUrl;
    }

    public String subscribe(String clientId ,String webSocketUrl) throws IOException, InterruptedException {
        WebSocketInformation webSocketInformation = new WebSocketInformation();
        webSocketInformation.setClientId(clientId);
        webSocketInformation.setWebSocketUrl(webSocketUrl);
        Gson gson = new Gson();

        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(gson.toJson(webSocketInformation)))
                .uri(URI.create("http://localhost:8081/transactionmanager/subscribe"))
                .setHeader("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

    public void convert(int id, String path) throws IOException, InterruptedException {
        Gson gson = new Gson();

        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(path))
                .uri(URI.create("http://localhost:8081/transactionmanager/reverse/"+id))
                .setHeader("Content-Type", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    }
}
