package tp.architecture.distribuee.back4front.model;

import lombok.Data;

@Data
public class Client {
    String clientId;
}
