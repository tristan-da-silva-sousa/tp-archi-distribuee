package tp.architecture.distribuee.back4front;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Back4frontApplication {

    public static void main(String[] args) {
        SpringApplication.run(Back4frontApplication.class, args);
    }

}
