package tp.architecture.distribuee.frontend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import tp.architecture.distribuee.frontend.model.Job;

@Repository
public interface ReverseRepository extends MongoRepository<Job,String>{
}
