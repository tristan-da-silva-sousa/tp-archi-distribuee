package tp.architecture.distribuee.frontend.service;


import com.google.gson.Gson;
import org.bson.types.ObjectId;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tp.architecture.distribuee.frontend.model.Job;
import tp.architecture.distribuee.frontend.model.Notification;
import tp.architecture.distribuee.frontend.model.StatusMessage;
import tp.architecture.distribuee.frontend.repository.ReverseRepository;

import java.util.List;
import java.util.logging.Logger;

@Component
public class StatusMessageConsumer {

    @Autowired
    ReverseRepository reverseRepository;

    @Autowired
    WebSocketInformationService webSocketInformationService;

    @Autowired
    NotificationService notificationService;

    final static Logger logger = Logger.getLogger("Consumer");
    @RabbitListener(queues = "${status.rabbitmq.queue}")
    public void receivedMessage(String msg1){
        logger.info("Received Message: " + msg1);
        Gson gson = new Gson();
        StatusMessage msg = gson.fromJson(msg1,StatusMessage.class);

        ObjectId objectId = msg.getJobId();

        Job job = getJobFromObjectId(objectId);
        logger.info("Received Job: " + job.toString());

        Notification notification = new Notification();
        notification.setClientId("read");
        notification.setPath(job.getPath());
        notification.setStatus(msg.getStatus());


        notificationService.produceMsg(gson.toJson(notification));
        logger.info("Received Job: " + "notification sent");

    }

    private Job getJobFromObjectId(ObjectId objectId) {
        List<Job> list = reverseRepository.findAll();
        for (Job job:list
        ) {
            if(job.getId().equals(objectId)){
                return job;
            }
        }
        return null;
    }



}
