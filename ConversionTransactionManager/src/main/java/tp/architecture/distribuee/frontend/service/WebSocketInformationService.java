package tp.architecture.distribuee.frontend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tp.architecture.distribuee.frontend.model.WebSocketInformation;
import tp.architecture.distribuee.frontend.repository.WebSocketInformationRepository;

import java.util.List;

@Service
public class WebSocketInformationService {

    @Autowired
    WebSocketInformationRepository webSocketInformationRepository;

    public WebSocketInformation addWebSocketInformation(WebSocketInformation webSocketInformation){
        Iterable<WebSocketInformation> webSocketInformations = webSocketInformationRepository.findAll();
        for (WebSocketInformation ws:
             webSocketInformations) {
            //client already exist
            if (ws.getClientId().equals(webSocketInformation.getClientId())){return null;}
        }
        return webSocketInformationRepository.save(webSocketInformation);
    }
}
