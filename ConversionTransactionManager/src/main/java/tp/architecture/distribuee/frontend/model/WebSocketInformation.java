package tp.architecture.distribuee.frontend.model;

import lombok.Data;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

@Data
@RedisHash("WebSocketInformation")
public class WebSocketInformation implements Serializable {

    @Id
    String id;
    String clientId;
    String webSocketUrl;
}
