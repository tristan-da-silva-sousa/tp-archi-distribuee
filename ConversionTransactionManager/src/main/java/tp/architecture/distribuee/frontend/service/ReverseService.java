package tp.architecture.distribuee.frontend.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import tp.architecture.distribuee.frontend.model.Job;
import tp.architecture.distribuee.frontend.model.Message;
import tp.architecture.distribuee.frontend.repository.ReverseRepository;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;


@Service
@Component
public class ReverseService {

    ReverseRepository reverseRepository;

    public ReverseService(ReverseRepository reverseRepository) {
        this.reverseRepository = reverseRepository;
    }

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${jsa.rabbitmq.reverse.order.exchange}")
    private String exchange;

    @Value("${jsa.rabbitmq.routingkey}")
    private String routingKey;

    public int produceMsg(Message msg){
        amqpTemplate.convertAndSend(exchange, routingKey, msg.toString());
        return msg.getId();
    }

    public Job produceJob(String method, String path){
        Job job = new Job(method,path,false,LocalDateTime.now());
        reverseRepository.insert(job);
        return job;
    }

    public boolean prepareFile(String path){
        String line = "This is a test of reverse";
        line +=System.lineSeparator();
        boolean isReadable = Files.isReadable(Paths.get(path));
        if(isReadable){
            return true;
        }else {
            try {
                FileWriter fileWriter = new FileWriter(path);
                fileWriter.write(line);
                fileWriter.close();
                return true;
            }catch (IOException io) {
                io.printStackTrace();
                return false;
            }
        }
    }
}
