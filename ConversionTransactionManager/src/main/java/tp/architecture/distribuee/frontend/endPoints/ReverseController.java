package tp.architecture.distribuee.frontend.endPoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tp.architecture.distribuee.frontend.model.Job;
import tp.architecture.distribuee.frontend.model.Message;
import tp.architecture.distribuee.frontend.model.WebSocketInformation;
import tp.architecture.distribuee.frontend.repository.WebSocketInformationRepository;
import tp.architecture.distribuee.frontend.service.ReverseService;
import tp.architecture.distribuee.frontend.service.WebSocketInformationService;

@RestController
@RequestMapping("/transactionmanager")
public class ReverseController {

    private final ReverseService reverseService;

    @Autowired
    private WebSocketInformationRepository webSocketInformationRepository;

    @Autowired
    private WebSocketInformationService webSocketInformationService;

    public ReverseController(ReverseService reverseService) {
        this.reverseService = reverseService;
    }


    @PostMapping("/reverse/{id}")
    public Integer reverse(@PathVariable("id") int id, @RequestBody  String path){
        boolean fileReady = reverseService.prepareFile(path);
        if (fileReady){
            Job job = reverseService.produceJob("reverse",path);
            Message msg = new Message();
            msg.setId(id);
            msg.setJobId(job.getId());
            int idMessage = reverseService.produceMsg(msg);
            return idMessage;
        }
        else{
            return null;
        }

    }

    @PostMapping("/subscribe")
    public String subscribe(@RequestBody WebSocketInformation webSocketInformation){
        WebSocketInformation ws = webSocketInformationService.addWebSocketInformation(webSocketInformation);
        if (ws==null){return "Already exist";}
        return ws.toString();
    }

}
