package tp.architecture.distribuee.frontend.model;

import lombok.Data;

import javax.persistence.Id;

@Data
public class Notification {

    @Id
    private String Id;
    private String path;
    private String clientId;
    private StatusMessage.Status status;



}
