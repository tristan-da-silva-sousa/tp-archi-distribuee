package tp.architecture.distribuee.frontend.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;


@Document(collection = "jobs")
public class Job {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    protected ObjectId id;

    @Field(value = "Method")
    private String method;

    @Field(value = "Path")
    private String path;

    @Field(value = "Done")
    private boolean done;

    @Field(value = "Date")
    private LocalDateTime date;

    public Job(String method,String path, boolean done, LocalDateTime date){
        this.method = method;
        this.path = path;
        this.done = done;
        this.date = date;
    }

    public Job() {
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", method='" + method + '\'' +
                ", path='" + path + '\'' +
                ", done=" + done +
                ", date=" + date +
                '}';
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public ObjectId getId() {
        return id;
    }

    public String getMethod() {
        return method;
    }

    public boolean isDone() {
        return done;
    }

    public LocalDateTime getDate() {
        return date;
    }

}
