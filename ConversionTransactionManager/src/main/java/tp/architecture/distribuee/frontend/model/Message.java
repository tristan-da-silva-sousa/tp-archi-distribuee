package tp.architecture.distribuee.frontend.model;

import org.bson.types.ObjectId;
import org.springframework.lang.NonNull;
import org.springframework.data.annotation.Id;

public class Message {
    @Id
    int id;
    @NonNull
    ObjectId jobId;


    public void setJobId(@NonNull ObjectId jobId) {
        this.jobId = jobId;
    }

    @NonNull
    public ObjectId getJobId() {
        return jobId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", jobId=" + jobId +
                '}';
    }
}
