package tp.architecture.distribuee.frontend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tp.architecture.distribuee.frontend.model.WebSocketInformation;

import java.util.Optional;

@Repository
public interface WebSocketInformationRepository extends CrudRepository<WebSocketInformation,String> {
}
