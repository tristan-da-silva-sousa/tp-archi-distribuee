package tp.architecture.distribuee.frontend.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import tp.architecture.distribuee.frontend.model.Message;
import tp.architecture.distribuee.frontend.model.Notification;


@Service
@Component
public class NotificationService {
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${jsa.rabbitmq.reverse.notification.exchange}")
    private String exchange;

    @Value("${jsa.rabbitmq.routingkey}")
    private String routingKey;

    public String produceMsg(String msg){
        amqpTemplate.convertAndSend(exchange, routingKey, msg.toString());
        return msg;
    }



}
