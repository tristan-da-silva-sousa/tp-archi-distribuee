package tp.archi.distribuee.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import tp.archi.distribuee.backend.model.Job;


@Repository
public interface ReverseRepository extends MongoRepository<Job,Long> {}
