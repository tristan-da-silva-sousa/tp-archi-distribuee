package tp.archi.distribuee.backend.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Document(collection = "jobs")
public class Job {

    @Id
    private ObjectId id;

    @Field(value = "Method")
    private String method;

    @Field(value = "Done")
    private boolean done;

    @Field(value = "Date")
    private LocalDateTime date;

    @Field(value = "Path")
    private String path;



    public Job(String method, boolean done, LocalDateTime date,String path){
        this.method = method;
        this.done = done;
        this.date = date;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", method='" + method + '\'' +
                ", done=" + done +
                ", date=" + date +
                ", path='" + path + '\'' +
                '}';
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public ObjectId getId() {
        return id;
    }

    public String getMethod() {
        return method;
    }

    public boolean isDone() {
        return done;
    }

    public LocalDateTime getDate() {
        return date;
    }
}
