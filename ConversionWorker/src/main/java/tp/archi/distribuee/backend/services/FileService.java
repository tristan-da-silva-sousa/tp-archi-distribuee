package tp.archi.distribuee.backend.services;

import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class FileService {

    public boolean reverseFile(String path) {
        File fileToBeModified = new File(path);
        String oldContent = "";
        BufferedReader reader = null;
        FileWriter writer = null;
        try
        {
            reader = new BufferedReader(new FileReader(fileToBeModified));
            String line = reader.readLine();
            while (line != null)
            {
                oldContent = oldContent + line + System.lineSeparator();
                line = reader.readLine();
            }

            StringBuffer sb = (new StringBuffer(oldContent)).reverse();
            String newContent = sb.toString();

            writer = new FileWriter(fileToBeModified);
            writer.write(newContent);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
        finally
        {
            try
            {
                reader.close();
                writer.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
