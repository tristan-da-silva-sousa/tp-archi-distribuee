package tp.archi.distribuee.backend.mqtt;

import lombok.extern.log4j.Log4j;
import org.bson.types.ObjectId;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tp.archi.distribuee.backend.model.Job;
import tp.archi.distribuee.backend.model.StatusMessage;
import tp.archi.distribuee.backend.repository.ReverseRepository;
import tp.archi.distribuee.backend.services.FileService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;

@Log4j
@Component
public class Consumer {

    @Autowired
    Publisher publisher;

    @Autowired
    ReverseRepository reverseRepository;
    @Autowired
    FileService fileService;

    final static Logger logger = Logger.getLogger("Consumer");
    @RabbitListener(queues = "${jsa.rabbitmq.queue}")
    public void receivedMessage(String msg){
        logger.info("Received Message: " + msg);


        ObjectId objectId = getObjectIdFromMessage(msg);

        Job job = getJobFromObjectId(objectId);
        logger.info("Received Job: " + job.toString());

        //Write in file
        String path= job.getPath();
        if(job.getMethod().equals("reverse")){
            boolean result = fileService.reverseFile(path);
            if(result){
                job.setDone(true);
                job.setDate(LocalDateTime.now());
                reverseRepository.save(job);
                publisher.produceMsg(new StatusMessage(objectId, StatusMessage.Status.DONE));
            }else {
                publisher.produceMsg(new StatusMessage(objectId, StatusMessage.Status.ERROR));
            }
        }else {
            logger.info("Unrecognized method \'"+job.getMethod()+"\'");
            publisher.produceMsg(new StatusMessage(objectId, StatusMessage.Status.ERROR));
        }



    }

    private Job getJobFromObjectId(ObjectId objectId) {
        List<Job> list = reverseRepository.findAll();
        for (Job job:list
        ) {
            if(job.getId().equals(objectId)){
                return job;
            }
        }
        return null;
    }

    private ObjectId getObjectIdFromMessage(String msg) {
        String sWithoutComa = msg.replace("}","=");
        String[] strings =  sWithoutComa.split("=");
        ObjectId objectId = new ObjectId(strings[2]);
        return objectId;
    }


}
