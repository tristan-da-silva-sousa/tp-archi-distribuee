package tp.archi.distribuee.backend.mqtt;

import com.google.gson.Gson;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import tp.archi.distribuee.backend.model.StatusMessage;


@Service
@Component
public class Publisher {

    public Publisher() {
    }

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${rabbitmq.exchange.status}")
    private String exchange;

    @Value("${jsa.rabbitmq.routingkey}")
    private String routingKey;

    public int produceMsg(StatusMessage msg){
        Gson gson = new Gson();

        amqpTemplate.convertAndSend(exchange, routingKey, gson.toJson(msg));
        return msg.getId();
    }


}
