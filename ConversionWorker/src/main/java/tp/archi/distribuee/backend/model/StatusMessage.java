package tp.archi.distribuee.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.lang.NonNull;

@Data
public class StatusMessage {

    @Id
    int id;
    @NonNull
    ObjectId jobId;

    Status status;

    public enum Status {
        DONE, ERROR, STARTED
    }

    public StatusMessage(@NonNull ObjectId jobId, Status status) {
        this.jobId = jobId;
        this.status = status;
    }
}
