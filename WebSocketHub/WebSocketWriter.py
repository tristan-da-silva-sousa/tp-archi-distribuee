import asyncio
import websockets
import sys
#add the client Id as argument

async def hello():
    async with websockets.connect('ws://localhost:5678/status-episen-write/'+sys.argv[1]) as websocket:
        text = input("Message ? ")
        await websocket.send(text)
 
asyncio.get_event_loop().run_until_complete(hello())