import asyncio
import datetime
import random
import websockets
import re

dico_websocket = {}

 
async def pub_sub(websocket, path):#new connection
    print(path)
    global dico_websocket
    splitted_list = path.split("/")         #the las element is the clientid


    if re.search("^/status-episen-read/.*", path) :     #Reader
        print("Reader trying to connect")


        if path in dico_websocket:                      #Reader with a created path can wait messages
            print("Instantiated connection thank to existing object")
            dico_websocket[path].add(websocket)
            print("READER "+str(websocket.remote_address)+"    connected for "+path)
            while True:
                await asyncio.sleep(100)


        else:                                           #Reader without a created path is disconnected
            print("Not instantiated connection")
            #await asyncio.sleep(100)
            await websocket.send("Not instantiated connection")
            websocket.close()
        #connected.add(websocket)
        
    elif re.search("^/status-episen-write/.*", path) :  #Writer
        print("WRITER "+str(websocket.remote_address)+"    connected for "+str(splitted_list[-1]))


        try :
            while True:                                 #Writer always can send message
                data = await websocket.recv()
                print("MULTICAST: "+data)
                still_connected = set()
                connected_list = dico_websocket["/status-episen-read/"+str(splitted_list[-1])]
                for ws in connected_list : 
                                 #Select reader concerned by the subject
                    if ws.open:                         #Chek if reader a still connected
                        still_connected.add(ws)
                        await asyncio.wait([ws.send(data)])

                    else:
                        print("READER "+str(ws.remote_address)+" disconnected")     
                dico_websocket["/status-episen-read/"+str(splitted_list[-1])]=still_connected   #keep only connected reader

        except:
            print("WRITER "+str(websocket.remote_address)+" disconnected")  #end of write process


    elif re.search("^/create/.*", path) :                       #Creator 
        print("Create endpoint")

        if path in dico_websocket:                              #Add the client to a existing topic
            print("This client already exist")
            dico_websocket[path].add(websocket)
            await websocket.send("/status-episen-read/"+splitted_list[-1])

        else:
            print("Create a new access")                        #Add the client to a new topic
            new_path = "/status-episen-read/"+splitted_list[-1]
            dico_websocket[new_path] = {websocket}
            await websocket.send(new_path)


    else:                                                       #The client don't use a correct path
        print("Forbidden access")
             
start_server = websockets.serve(pub_sub, '127.0.0.1', 5678)     #Run on localhost:5678
 
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()