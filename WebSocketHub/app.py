import subprocess
from flask import Flask
from flask import request
app = Flask(__name__)


@app.route("/websocket/ws-admin/create", methods=['POST'])
def hello():
	clientid = request.json
	response = "ws://localhost:5678/status-episen-read/"+str(clientid)
	cmd = 'python WebSocketCreator.py '+str(clientid)
	process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	process.communicate()
	return response

if __name__ == "__main__" :
	app.run(debug=True, host="localhost", port=5000)
