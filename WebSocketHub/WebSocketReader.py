#!/usr/bin/env python3
#add the client Id as argument
import asyncio
import websockets
import sys
 
async def hello():
    async with websockets.connect('ws://localhost:5678/status-episen-read/'+str(sys.argv[1])) as websocket:
        while True:
            name = await websocket.recv()
            print("RECEIVED: "+name)
 
asyncio.get_event_loop().run_until_complete(hello())