#!/usr/bin/env python3
 
import asyncio
import websockets
#add the client Id as argument



async def hello(clientid):
    async with websockets.connect('ws://localhost:5678/create/'+str(clientid)) as websocket:
        name = await websocket.recv()
        print("RECEIVED URI CONNECTION: "+name)
        return name


def launch(path):
    asyncio.get_event_loop().run_until_complete(hello(path))
    

launch("read")